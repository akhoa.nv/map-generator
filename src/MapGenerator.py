import random
from collections import namedtuple, deque
from copy import deepcopy
from math import sqrt, ceil

Point = namedtuple('Point', 'x y')
OceanWorkUnit = namedtuple('OceanWorkUnit', 'point depth')
LandWorkUnit = namedtuple('LandWorkUnit', 'point value')


class MapGenerator:
    DIRECTIONS4 = [Point(-1, 0), Point(1, 0), Point(0, -1), Point(0, 1)]
    DIRECTIONS8 = [Point(-1, 0), Point(1, 0), Point(0, -1), Point(0, 1), Point(-1, -1), Point(-1, 1), Point(1,-1), Point(1,1)]

    def __init__(self, width, height, seed_value=0):
        self.width = width
        self.height = height
        self.ocean_depth_ratio = 0.2
        self.land_increase_ratio=0.3
        if seed_value != 0:
            random.seed(seed_value)
        else:
            random.seed()

    def generate_map(self, ocean_depth_ratio=0.2, land_increase_ratio=0.3,
                     doNormalize:bool=True, method='bottom_up', peak_count=5,
                     doGenerateTerrain:bool=False):
        self.ocean_depth_ratio = ocean_depth_ratio
        self.land_increase_ratio = land_increase_ratio
        smallest_side = min(self.width, self.height)
        ocean_depth = int(smallest_side / 2 * ocean_depth_ratio)
        print('\nMax Ocean Depth: {}\nGenerating Map...'.format(ocean_depth))
        elevation_map = []
        terrain_map = []
        # Generate empty map
        for x in range(self.width):
            elevation_map.append([0] * self.height)

        work_list = self.place_ocean(elevation_map, ocean_depth)
        # Make a copy of elevation_map for terrain_map
        terrain_map = deepcopy(elevation_map)
        peaks = []
        if method == 'top_down':
            peaks, max_height = self.place_land_top_down(elevation_map, land_increase_ratio, peak_count=peak_count)
        else:
            peaks, max_height = self.place_land_bottom_up(elevation_map, work_list, land_increase_ratio)

        river_positions = self.place_rivers_and_lakes_randomly(elevation_map, peaks, 0.7)
        if doGenerateTerrain:
            self.generate_terrain(elevation_map, terrain_map, river_positions, max_height, 4)
            self.normalize(terrain_map, doNormalize=doNormalize)
        self.normalize(elevation_map, doNormalize=doNormalize)
        print('Done!')
        if doGenerateTerrain:
            return terrain_map
        else:
            return elevation_map

    def place_ocean(self, current_map, max_depth=1):
        visited = []
        work_queue = deque()
        starting_land_queue = deque()

        # Add top row
        for x in range(self.width):
            position = Point(x, 0)
            work_queue.append(OceanWorkUnit(position, 1))
        # Add bottom row
        for x in range(self.width):
            position = Point(x, self.height - 1)
            work_queue.append(OceanWorkUnit(position, 1))

        # Added Left Column
        for y in range(1, self.height - 1):
            position = Point(0, y)
            work_queue.append(OceanWorkUnit(position, 1))

        # Added Right Column
        for y in range(1, self.height - 1):
            position = Point(self.width - 1, y)
            work_queue.append(OceanWorkUnit(position, 1))

        while len(work_queue) > 0:
            position, depth = work_queue.popleft()
            if position in visited:
                continue
            # Mark visited
            visited.append(position)
            # Mark on map
            current_map[position.x][position.y] = 1
            # Add neighbors (base on chance)
            neighbors = []
            for offset in MapGenerator.DIRECTIONS4:
                new_x = position.x + offset.x
                new_y = position.y + offset.y
                # Out of bound
                if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                    continue
                neighbors.append((new_x, new_y))

            while len(neighbors) > 0:
                # Get random neighbor
                index = random.randrange(0, len(neighbors))
                new_x, new_y = neighbors[index]
                del neighbors[index]
                if random.randrange(0, max_depth) >= depth:
                    if Point(new_x, new_y) in visited:
                        continue
                    position = Point(new_x, new_y)
                    work_queue.append(OceanWorkUnit(position, depth + 1))
                else:
                    if Point(new_x, new_y) in visited:
                        continue
                    position = Point(new_x, new_y)
                    starting_land_queue.append(LandWorkUnit(position, 2))
        return starting_land_queue

    def place_land_bottom_up(self, current_map, starting_work_list, increase_factor):
        work_list = deepcopy(starting_work_list)
        maxValue = 2
        peaks = []

        while len(work_list) > 0:
            # Get random workload
            index = random.randrange(0, len(work_list))
            position, value = work_list[index]
            del work_list[index]
            # Only work on visited node
            if current_map[position.x][position.y] == 0:
                # Set value on map
                current_map[position.x][position.y] = value
                neighbors = []
                # Add neighbors if they are not visited yet
                for offset in MapGenerator.DIRECTIONS4:
                    new_x = position.x + offset.x
                    new_y = position.y + offset.y
                    # Out of bound
                    if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                        continue
                    neighbors.append((new_x, new_y))

                while len(neighbors) > 0:
                    # Get random neighbor
                    index = random.randrange(0, len(neighbors))
                    new_x, new_y = neighbors[index]
                    del neighbors[index]
                    # If neighbor is unvisited
                    if current_map[new_x][new_y] == 0:
                        position = Point(new_x, new_y)
                        new_value = value
                        if random.randrange(1, 100) >= (increase_factor * 100):
                            new_value = value + 1
                        if new_value == maxValue:
                            peaks.append(position)
                        elif new_value > maxValue:
                            maxValue = new_value
                            peaks = [position]
                        work_list.append(LandWorkUnit(position, new_value))
        peaks = set(peaks)
        peaks = list(peaks)
        return peaks, maxValue

    def place_land_top_down(self, current_map, increase_factor, peak_count=5):
        peaks = self.place_peak_positions(current_map, peak_count)
        work_list = deque()

        maxValue = 2

        # Add available neighbors off peaks to work list
        for x, y in peaks:
            neighbors = []
            for offset in MapGenerator.DIRECTIONS4:
                new_x = x + offset.x
                new_y = y + offset.y
                # Out of bound
                if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                    continue
                neighbors.append((new_x, new_y))

            while len(neighbors) > 0:
                # Get random neighbor
                index = random.randrange(0, len(neighbors))
                new_x, new_y = neighbors[index]
                del neighbors[index]
                if current_map[new_x][new_y] == 0:
                    position = Point(new_x, new_y)
                    new_value = 2
                    if random.randrange(1, 100) >= (increase_factor * 100):
                        new_value = 3
                    if new_value > maxValue:
                        maxValue = new_value
                    work_list.append(LandWorkUnit(position, new_value))

        # Start generating the map
        while len(work_list) > 0:
            # Get random workload
            index = random.randrange(0, len(work_list))
            position, value = work_list[index]
            del work_list[index]
            # Only work on visited node
            if current_map[position.x][position.y] == 0:
                # Set value on map
                current_map[position.x][position.y] = value
                neighbors = []
                # Add neighbors if they are not visited yet
                for offset in MapGenerator.DIRECTIONS4:
                    new_x = position.x + offset.x
                    new_y = position.y + offset.y
                    # Out of bound
                    if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                        continue
                    neighbors.append((new_x, new_y))

                while len(neighbors) > 0:
                    # Get random neighbor
                    index = random.randrange(0, len(neighbors))
                    new_x, new_y = neighbors[index]
                    del neighbors[index]
                    # If neighbor is unvisited
                    if current_map[new_x][new_y] == 0:
                        position = Point(new_x, new_y)
                        new_value = value
                        if random.randrange(1, 100) >= (increase_factor * 100):
                            new_value = value + 1
                        if new_value > maxValue:
                            maxValue = new_value
                        work_list.append(LandWorkUnit(position, new_value))

        # Inverse the map
        new_values = {}
        values = range(2, maxValue+1)
        for i in range(len(values)):
            new_values[values[i]] = values[-(i+1)]
        for x in range(self.width):
            for y in range(self.height):
                if current_map[x][y] > 1:
                    current_map[x][y] = new_values[current_map[x][y]]
                if current_map[x][y] == 0:
                    current_map[x][y] = 2
        return peaks, maxValue

    def place_peak_positions(self, current_map, max_peak_count):
        peak_count = random.randint(1, max_peak_count)
        peaks = []
        smallest_side = min(self.width, self.height)
        minimum_distance = smallest_side / peak_count * 0.6
        maximum_distance = smallest_side / peak_count * 1.5
        while len(peaks) < peak_count:
            x = random.randrange(0, self.width)
            y = random.randrange(0, self.height)

            # Position not available
            if current_map[x][y] != 0:
                continue

            # If this is our first peak, just add it to the list
            if len(peaks) == 0:
                current_map[x][y] = 2
                peaks.append(Point(x, y))
                continue

            isValid = True
            for peak_x, peak_y in peaks:
                euclid_dist = sqrt((x - peak_x)**2 + (y - peak_y)**2)
                if euclid_dist < minimum_distance:
                    peak_count -= 1
                    isValid = False
                    break
                if euclid_dist > maximum_distance:
                    peak_count -= 1
                    isValid = False
                    break

            if isValid:
                current_map[x][y] = 2
                peaks.append(Point(x, y))
        return peaks

    def place_rivers_and_lakes(self, current_map, starting_points, river_width, lake_width, river_factor=0.5):
        river_positions = []
        for starting_point in starting_points:
            work_queue = deque()
            next_work_queue = deque()
            frontier = []
            new_x = random.randrange(0, self.width)
            new_y = random.randrange(0, self.height)
            work_queue.append(Point(new_x, new_y))
            frontier.append(Point(new_x, new_y))

            # Place river using BFS
            while len(work_queue) > 0:
                position = work_queue.popleft()
                # Check neighbors for possible route
                neighbors = []
                for offset in MapGenerator.DIRECTIONS4:
                    new_x = position.x + offset.x
                    new_y = position.y + offset.y
                    # Out of bound
                    if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                        continue
                    if Point(new_x, new_y) in river_positions:
                        continue
                    if current_map[new_x][new_y] == 1:
                        continue
                    if Point(new_x, new_y) in frontier:
                        continue
                    # Only allow river to go down elevation
                    if current_map[new_x][new_y] >= current_map[position.x][position.y]:
                        if random.randint(0, 100) <= (100 * river_factor):
                            work_queue.append(Point(new_x, new_y))
                            frontier.append(Point(new_x, new_y))

                current_map[position.x][position.y] = 1

    def place_rivers_and_lakes_randomly(self, current_map, starting_points, river_factor=0.5):
        river_positions = []
        num_rivers_per_peak = 1
        if len(starting_points) <= 2:
            num_rivers_per_peak = 3
        for position in starting_points:
            for i in range(num_rivers_per_peak):
                # Diagonal
                isDiagonal = True
                if random.randint(0, 1) == 1:
                    isDiagonal = False

                new_x = random.choice([-1, 1])
                new_y = random.choice([-1, 1])
                directions = [Point(new_x, 0), Point(0, new_y)]

                while True:
                    if isDiagonal:
                        offset = random.choice(directions)
                    else:
                        p = 0
                        notP = 1
                        if random.randint(0, 1) == 0:
                            p = 0.7
                            notP = 1 - p
                        else:
                            p = 0.3
                            notP = 1 - p
                        offset = random.choices(population=directions, weights=[p, notP], k=1)[0]
                    new_x = position.x + offset.x
                    new_y = position.y + offset.y
                    # Out of bound
                    if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                        continue
                    if current_map[position.x][position.y] == 1:
                        break
                    river_positions.append(position)
                    current_map[position.x][position.y] = 1
                    position = Point(new_x, new_y)
        return river_positions

    def generate_terrain(self, elevation_map, terrain_map, river_positions, max_height, river_width=1):
        # Placing the river down with grassland
        for position in river_positions:
            terrain_map[position.x][position.y] = 1
            for offset in MapGenerator.DIRECTIONS4:
                for i in range(river_width):
                    new_x = position.x + (offset.x * (1 + i))
                    new_y = position.y + (offset.y * (1 + i))
                    # Out of bound
                    if (new_x < 0) or (new_x >= self.width) or (new_y < 0) or (new_y >= self.height):
                        continue
                    if Point(new_x, new_y) in river_positions:
                        continue
                    if terrain_map[new_x][new_y] == 1:
                        continue
                    terrain_map[new_x][new_y] = 2
        # Place mountain area
        mountain_cutoff = int(max_height * 0.7)
        # Place desert for the rest of map
        for x in range(self.width):
            for y in range(self.height):
                if terrain_map[x][y] == 0:
                    if elevation_map[x][y] >= mountain_cutoff:
                        terrain_map[x][y] = 3
                    else:
                        terrain_map[x][y] = 4

    def normalize(self, current_map, doNormalize=True):
        maxValue = 0
        for x in range(self.width):
            for y in range(self.height):
                current_map[x][y] -= 1
                if current_map[x][y] > maxValue:
                    maxValue = current_map[x][y]

        multiFactor = int(255 / maxValue)
        if doNormalize:
            for x in range(self.width):
                for y in range(self.height):
                    current_map[x][y] *= multiFactor
