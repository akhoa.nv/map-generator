from src.MapGenerator import MapGenerator

map_generator = MapGenerator(12, 12)
new_map = map_generator.generate_map(0.3, 0.3, False, method='top_down')
for x in range(map_generator.width):
    print(new_map[x])
