import pygame, math, sys, random
from pygame.locals import *
from MapGenerator import *

class Board:
    def __init__(self, pixelSize, dataW, dataH, oceanDepth, landIncrease, isNormalize):
        self.objArr = None
        self.DATAW = dataW
        self.DATAH = dataH
        self.OCEANDEPTH = oceanDepth
        self.LANDINCREASE = landIncrease
        self.NORMALIZE = isNormalize
        self.PIXEL_SIZE = pixelSize

        self.BG = (255, 255, 255)
        self.SEA = (0, 119, 190)
        self.WHITE = (255, 255, 255)
        self.BLACK = (0, 0, 0)

        self.screen = pygame.display.set_mode((self.PIXEL_SIZE * dataW, self.PIXEL_SIZE * dataH))

        self.RECREATE = True
        self.GRID = False
        self.GRID_CHANGES = False
        self.OPTION = False
        self.SEED = "0"
        self.VAL_OCEAND = ""
        self.VAL_LANDI = ""
        self.UI_SEED = False
        self.UI_OCEAND = False
        self.UI_LANDI = False

        pygame.mixer.init()
        pygame.font.init()
        self.NORMALFONT = pygame.font.SysFont('Arial', 15)
        self.HEADERFONT = pygame.font.SysFont('Arial', 30, True)

        self.clock = pygame.time.Clock()  # load clock

        self.play = True

        self.main()

    def main(self):
        while self.play:
            # USER INPUT
            self.clock.tick(60)

            # get events from the user
            for event in pygame.event.get():  # check if presses a key or left it
                if event.type == pygame.QUIT:
                    sys.exit()

                # not a key event
                if not hasattr(event, 'key'):
                    continue
                if self.OPTION:
                    if event.type == KEYDOWN:
                        if event.key == K_0 or \
                                event.key == K_1 or \
                                event.key == K_2 or \
                                event.key == K_3 or \
                                event.key == K_4 or \
                                event.key == K_5 or \
                                event.key == K_6 or \
                                event.key == K_7 or \
                                event.key == K_8 or \
                                event.key == K_9:
                            if self.UI_OCEAND:
                                self.VAL_OCEAND = self.VAL_OCEAND + pygame.key.name(event.key)
                            elif self.UI_LANDI:
                                self.VAL_LANDI = self.VAL_LANDI + pygame.key.name(event.key)
                            elif self.UI_SEED:
                                self.SEED = self.SEED + pygame.key.name(event.key)
                        elif event.key == K_PERIOD:
                            if self.UI_OCEAND:
                                self.VAL_OCEAND = self.VAL_OCEAND + pygame.key.name(event.key)
                            elif self.UI_LANDI:
                                self.VAL_LANDI = self.VAL_LANDI + pygame.key.name(event.key)
                        elif event.key == K_BACKSPACE:
                            if self.UI_OCEAND:
                                self.VAL_OCEAND = self.VAL_OCEAND[0: -1]
                            elif self.UI_LANDI:
                                self.VAL_LANDI = self.VAL_LANDI[0: -1]
                            elif self.UI_SEED:
                                self.SEED = self.SEED[0: -1]
                        elif event.key == K_RETURN:
                            if self.UI_OCEAND:
                                self.UI_OCEAND = False
                                self.UI_LANDI = True
                                if not (self.VAL_OCEAND == ""):
                                    try:
                                        self.OCEANDEPTH = float(self.VAL_OCEAND)
                                    except ValueError:
                                        pass
                            elif self.UI_LANDI:
                                self.UI_LANDI = False
                                if not (self.VAL_LANDI == ""):
                                    try:
                                        self.LANDINCREASE = float(self.VAL_LANDI)
                                    except ValueError:
                                        pass
                            elif self.UI_SEED:
                                if self.SEED == "":
                                    self.SEED = "0"
                                self.UI_SEED = False
                                self.UI_OCEAND = True

                else:
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            self.play = False
                        if event.key == K_r:
                            self.RECREATE = True
                        if event.key == K_g:
                            self.RECREATE = True
                            self.GRID_CHANGES = self.GRID
                            self.GRID = not self.GRID
                        if event.key == K_s:
                            self.SEED = ""
                            self.VAL_OCEAND = str(self.OCEANDEPTH)
                            self.VAL_LANDI = str(self.LANDINCREASE)
                            self.UI_SEED = True
                            self.OPTION = True


            if self.RECREATE:
                self.screen.fill(self.BG)
                if self.GRID == self.GRID_CHANGES:
                    self.objArr = MapGenerator(self.DATAW, self.DATAH, int(self.SEED)).generate_map(self.OCEANDEPTH, self.LANDINCREASE, self.NORMALIZE)
                for x in range(0, len(self.objArr)):
                    for y in range(0, len(self.objArr[x])):
                        color = self.WHITE
                        border = self.WHITE

                        if self.objArr[x][y] == 0:
                            color = self.SEA
                        else:
                            color = (self.objArr[x][y],
                                     255 - self.objArr[x][y], 0)


                        if self.GRID:
                            border = self.BLACK
                        else:
                            border = color

                        rect = pygame.Rect(x * self.PIXEL_SIZE, y * self.PIXEL_SIZE, self.PIXEL_SIZE, self.PIXEL_SIZE)
                        self.draw_rect(self.screen, color, border, rect, 1)
                self.RECREATE = False

                if not (self.GRID == self.GRID_CHANGES):
                    print("\nGrid is set to " + str(self.GRID))
                    self.GRID_CHANGES = self.GRID
            elif self.OPTION:
                self.screen.fill(self.BG)
                header = self.HEADERFONT.render("PREFERENCES", True, self.BLACK)
                seed_header = self.NORMALFONT.render("Seed number :", True, self.BLACK)
                seed = self.NORMALFONT.render(self.SEED, True, self.BLACK)

                self.screen.blit(header, (20, 20))
                self.screen.blit(seed_header, (20, 70))
                self.screen.blit(seed, (105, 70))

                if self.UI_OCEAND:
                    oceand_header = self.NORMALFONT.render("Ocean Depth :", True, self.BLACK)
                    oceand = self.NORMALFONT.render(self.VAL_OCEAND, True, self.BLACK)

                    self.screen.blit(oceand_header, (20, 90))
                    self.screen.blit(oceand, (105, 90))
                elif self.UI_LANDI:
                    oceand_header = self.NORMALFONT.render("Ocean Depth :", True, self.BLACK)
                    oceand = self.NORMALFONT.render(self.VAL_OCEAND, True, self.BLACK)
                    landi_header = self.NORMALFONT.render("Land Increase :", True, self.BLACK)
                    landi = self.NORMALFONT.render(self.VAL_LANDI, True, self.BLACK)

                    self.screen.blit(oceand_header, (20, 90))
                    self.screen.blit(oceand, (105, 90))
                    self.screen.blit(landi_header, (20, 110))
                    self.screen.blit(landi, (110, 110))
                elif not self.UI_SEED and not self.UI_OCEAND and not self.UI_LANDI:
                    oceand_header = self.NORMALFONT.render("Ocean Depth :", True, self.BLACK)
                    oceand = self.NORMALFONT.render(self.VAL_OCEAND, True, self.BLACK)
                    landi_header = self.NORMALFONT.render("Land Increase :", True, self.BLACK)
                    landi = self.NORMALFONT.render(self.VAL_LANDI, True, self.BLACK)
                    generate = self.NORMALFONT.render("Generating map using params...", True, self.BLACK)

                    self.screen.blit(oceand_header, (20, 90))
                    self.screen.blit(oceand, (105, 90))
                    self.screen.blit(landi_header, (20, 110))
                    self.screen.blit(landi, (110, 110))
                    self.screen.blit(generate, (20, 140))

                    self.OPTION = False
                    self.RECREATE = True

            pygame.display.flip()

    def draw_rect(self, surface, fill_color, outline_color, rect, border=1):
        surface.fill(outline_color, rect)
        surface.fill(fill_color, rect.inflate(-border * 2, -border * 2))
        pygame.draw.rect(surface, outline_color, rect, 1)


Board(7, 100, 100, 0.3, 0.5, True)
